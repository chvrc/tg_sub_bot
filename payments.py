from yoomoney import Client
from yoomoney import Quickpay
token = "410014836540511.2CC3DF9BBF3A92EC6E9BDD95910237AD5A12C9BA86EA99AE0464ED9C40943285C483B35FD4D48479A613D86D14CA6CD1732BE30A7AAF44A3BA63CFB945B0A9AFF27629C3F489DF97FD61AB3DDA2C6A6B8BED81355CF3C43C7B68EC8B4779825442650168787B0812121C6C40DAF40A57D77D4CF83F0401876ADE55B3792F2169"
client = Client(token)
user = client.account_info()


def pay_subscribe(hash, price):
    quickpay = Quickpay(
                receiver="410014836540511",
                quickpay_form="shop",
                targets="Подписка",
                paymentType="SB",
                sum=int(price),
                label=f"{hash}"
                )
    return quickpay.redirected_url


def pay_subscribe_check(hash):
    history = client.operation_history(label=f"{hash}")
    status = False
    for operation in history.operations:
        if operation.status == 'success':
            status = True
    return status
