from aiogram.dispatcher.storage import FSMContext
from database import add_referal, check_referal, sub_check, sub_remain_time
from keyboards import kb_back, kb_chats, kb_post, kb_post_back, kb_start
from aiogram.dispatcher.filters.builtin import CommandStart
from load_bot import dp
from aiogram import types
from states import MainMenu, Posting


@dp.message_handler(CommandStart(), state='*')
async def on_start_action(message: types.Message, state: FSMContext):
    await state.finish()
    check = await check_referal(message.from_user.id)
    if not check and message.get_args():
        referal = message.get_args()
        await add_referal(referal, message.from_user.id)
    kb = await kb_start()
    await message.answer('Главное меню',
                            reply_markup=kb)


@dp.callback_query_handler(lambda c: c.data == 'back')
async def on_chats_action(call: types.CallbackQuery, state: FSMContext):
    await state.finish()
    kb = await kb_start()
    await call.message.edit_text('Главное меню',
                            reply_markup=kb)


@dp.callback_query_handler(lambda c: c.data == 'rules')
async def on_chats_action(call: types.CallbackQuery, state: FSMContext):
    kb = await kb_back()
    await call.message.edit_text('1. Содержание объявлений не должно противоречить Федеральным законам Российской Федерации.',
                            reply_markup=kb)


@dp.callback_query_handler(lambda c: c.data == 'back', state=Posting.text)
@dp.callback_query_handler(lambda c: c.data == 'back', state=MainMenu.subscribe)
@dp.callback_query_handler(lambda c: c.data == 'groups')
async def on_chats_action(call: types.CallbackQuery, state: FSMContext):
    kb = await kb_chats()
    await call.message.edit_text('Выберите группу для размещения объявления',
                            reply_markup=kb)
    await state.finish()


@dp.callback_query_handler(lambda c: c.data.startswith('chat'))
async def on_chats_action(call: types.CallbackQuery, state: FSMContext):
    async with state.proxy() as data:
        data['chat'] = call.data[4:]
    check = await sub_check(call.from_user.id, call.data[4:])
    if check:
        db = await sub_remain_time(call.from_user.id, call.data[4:])
        kb = await kb_post_back(db)
        await call.message.edit_text('Введите текст объявления', reply_markup=kb)
        await Posting.text.set()
    else:
        kb = await kb_post()
        await call.message.edit_text('<b>У вас нет подписки!</b> Для размещения объявлений купите подписку.\n<b>Важно! Подписка на каждый канал покупается отдельно!</b>',
                                reply_markup=kb, parse_mode='HTML')
        await MainMenu.subscribe.set()
