from aiogram.dispatcher.storage import FSMContext
from database import check_referal, edit_referal
from keyboards import kb_back, kb_referal
from load_bot import dp, bot
from aiogram import types
from states import MainMenu, Posting


@dp.callback_query_handler(lambda c: c.data == 'referals', state='*')
async def on_chats_action(call: types.CallbackQuery, state: FSMContext):
    await state.finish()
    db = await check_referal(call.from_user.id)
    if db:
        count = db[0][2]
    else:
        count = 0
    kb = await kb_back()
    uid = call.from_user.id
    bbot = await bot.get_me()
    msg = f'\nКоличество бесплатных объявлений: {count}\nПригласите человека по вашей реферальной ссылке, чтобы получить бесплатное размещение объявления\nВаша реферальная ссылка:\nhttps://t.me/{bbot.username}?start={uid}'
    await call.message.edit_text(msg,
                            reply_markup=kb, parse_mode='HTML')


@dp.callback_query_handler(lambda c: c.data == 'referal', state=MainMenu.subscribe)
async def on_chats_action(call: types.CallbackQuery, state: FSMContext):
    db = await check_referal(call.from_user.id)
    if db:
        if db[0][2] > 0:
            kb = await kb_back()
            await call.message.edit_text('Введите текст объявления', reply_markup=kb)
            await edit_referal(call.from_user.id)
            await Posting.text.set()
        else:
            kb = await kb_referal()
            await call.message.edit_text('У вас нет бесплатных размещений.\nПригласите рефералов, чтобы получить бесплатные размещения', reply_markup=kb)
    else:
        kb = await kb_referal()
        await call.message.edit_text('У вас нет бесплатных размещений.\nПригласите рефералов, чтобы получить бесплатные размещения', reply_markup=kb)