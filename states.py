from aiogram.dispatcher.filters.state import State, StatesGroup


class MainMenu(StatesGroup):
    start = State()
    chats = State()
    rules = State()
    subscribe = State()


class Posting(StatesGroup):
    text = State()
    media = State()


class Admin(StatesGroup):
    start = State()
    chat = State()
    add_chat = State()
    delete_chat = State()
    price = State()
    price_chat = State()
    add_price = State()
    delete_price = State()