from aiogram.types.message import ContentType
from aiogram.types.reply_keyboard import ReplyKeyboardRemove
from keyboards import kb_posted, kb_skip
from load_bot import dp, bot
from states import Posting
from database import get_chat_id
from aiogram import types
from aiogram.dispatcher import FSMContext


@dp.message_handler(state=Posting.text)
async def post_message(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['text'] = message.text
    kb = await kb_skip()
    await message.answer('Отправьте изображение, которое будет прикреплено к посту или нажмите "Пропустить"',
                            reply_markup=kb)
    await Posting.media.set()


@dp.message_handler(lambda message: message.text == 'Пропустить', state=Posting.media)
async def post_message(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        text = data['text']
        chat_id = await get_chat_id(data['chat'])
    await bot.send_message(chat_id, text)
    kb = await kb_posted(chat_id)
    await message.answer(text, reply_markup=ReplyKeyboardRemove())
    await message.answer('Опубликовано!', reply_markup=kb)
    await state.finish()


@dp.message_handler(state=Posting.media, content_types=ContentType.PHOTO)
async def post_message(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        text = data['text']
        chat_id = await get_chat_id(data['chat'])
    await bot.send_photo(chat_id=chat_id, photo=message.photo[-1].file_id, caption=text)
    kb = await kb_posted(chat_id)
    await message.answer_photo(photo=message.photo[-1].file_id, caption=text, reply_markup=ReplyKeyboardRemove())
    await message.answer(f'Опубликовано!', reply_markup=kb)
    await state.finish()
