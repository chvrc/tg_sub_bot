from database import add_chat, add_price, del_chat, del_price, get_chats, get_prices
from aiogram.dispatcher.storage import FSMContext
from states import Admin
from keyboards import kb_admin, kb_back, kb_chats, kb_edit, kb_prices
from load_bot import ADMIN_ID, dp, bot
from aiogram import types
from aiogram.dispatcher.filters import Command


@dp.message_handler(Command('admin'), user_id=ADMIN_ID)
async def admin_start(message: types.Message):
    kb = await kb_admin()
    await message.answer('Админ-панель', reply_markup=kb)


@dp.callback_query_handler(lambda c: c.data == 'back', state=Admin.chat)
@dp.callback_query_handler(lambda c: c.data == 'back', state=Admin.price)
@dp.callback_query_handler(lambda c: c.data == 'admin')
async def admin_start_call(call: types.CallbackQuery, state: FSMContext):
    kb = await kb_admin()
    await call.message.edit_text('Админ-панель', reply_markup=kb)
    await state.finish()


@dp.callback_query_handler(lambda c: c.data == 'admin_exit')
async def admin_exit(call: types.CallbackQuery, state: FSMContext):
    await bot.delete_message(call.message.chat.id, call.message.message_id)
    await state.finish()


"""
        Чаты
"""
@dp.callback_query_handler(lambda c: c.data == 'back', state=Admin.add_chat)
@dp.callback_query_handler(lambda c: c.data == 'back', state=Admin.delete_chat)
@dp.callback_query_handler(lambda c: c.data == 'admin_chat')
async def admin_chats(call: types.CallbackQuery):
    db = await get_chats()
    kb = await kb_edit()
    if db:
        ans = 'Список групп:'
        for i in db:
            ans += f'\n{i[1]}'
    else:
        ans = 'Список групп пуст'
    await call.message.edit_text(ans, reply_markup=kb)
    await Admin.chat.set()


@dp.callback_query_handler(lambda c: c.data == 'add', state=Admin.chat)
async def add_chatf(call: types.CallbackQuery):
    kb = await kb_back()
    await call.message.edit_text('Введите название группы и ID группы через Enter. Например:\nГруппа 1\n123456788', reply_markup=kb)
    await Admin.add_chat.set()


@dp.message_handler(state=Admin.add_chat)
async def add_chatf2(message: types.Message):
    text = message.text.split('\n')
    if len(text) == 2:
        if text[1].isdigit():
            db = await add_chat(text[0], text[1])
            await add_price(db[-1][0], 1, 2)
            kb = await kb_edit()
            if db:
                ans = 'Список групп:'
                for i in db:
                    ans += f'\n{i[1]}'
            else:
                ans = 'Список групп пуст'
            await message.answer(ans, reply_markup=kb)
            await Admin.chat.set()
        else:
            await message.answer('ID должно быть числом')
    else:
        await message.answer('Неправильный ввод!\nВведите название группы и ID группы через Enter. Например:\nГруппа 1\n123456788')


@dp.callback_query_handler(lambda c: c.data == 'delete', state=Admin.chat)
async def del_chatf(call: types.CallbackQuery):
    kb = await kb_chats()
    await call.message.edit_text('Выберите группу, которую нужно удалить', reply_markup=kb)
    await Admin.delete_chat.set()


@dp.callback_query_handler(lambda c: c.data.startswith('chat'), state=Admin.delete_chat)
async def del_chatf(call: types.CallbackQuery):
    db = await del_chat(call.data[4:])
    kb = await kb_edit()
    if db:
        ans = 'Список групп:'
        for i in db:
            ans += f'\n{i[1]}'
    else:
        ans = 'Список групп пуст'
    await call.message.edit_text(ans, reply_markup=kb)
    await Admin.chat.set()


"""
        Подписки, цены, время подписки
"""
@dp.callback_query_handler(lambda c: c.data == 'back', state=Admin.price_chat)
@dp.callback_query_handler(lambda c: c.data == 'admin_price')
async def admin_chats(call: types.CallbackQuery):
    kb = await kb_chats()
    await call.message.edit_text('Выберите чат для редактирования цен', reply_markup=kb)
    await Admin.price.set()


@dp.callback_query_handler(lambda c: c.data.startswith('chat'), state=Admin.price)
async def del_chatf(call: types.CallbackQuery, state: FSMContext):
    async with state.proxy() as data:
        data['chat'] = call.data[4:]
    db = await get_prices(call.data[4:])
    if db:
        ans = 'Список цен на подписки:'
        for i in db:
            ans += f'\n{i[2]} дн. - {i[3]} руб.'
    else:
        ans = 'Список цен пуст'
    kb = await kb_edit()
    await call.message.edit_text(ans, reply_markup=kb)
    await Admin.price_chat.set()


@dp.callback_query_handler(lambda c: c.data == 'back', state=Admin.delete_price)
@dp.callback_query_handler(lambda c: c.data == 'back', state=Admin.add_price)
async def del_chatf(call: types.CallbackQuery, state: FSMContext):
    async with state.proxy() as data:
        db = await get_prices(data['chat'])
    if db:
        ans = 'Список цен на подписки:'
        for i in db:
            ans += f'\n{i[2]} дн. - {i[3]} руб.'
    else:
        ans = 'Список цен пуст'
    kb = await kb_edit()
    await call.message.edit_text(ans, reply_markup=kb)
    await Admin.price_chat.set()


@dp.callback_query_handler(lambda c: c.data == 'add', state=Admin.price_chat)
async def add_chatf(call: types.CallbackQuery):
    kb = await kb_back()
    await call.message.edit_text('Отправьте через пробел время подписки в днях и стоимость\nНапример: 7 100',
                                    reply_markup=kb)
    await Admin.add_price.set()


@dp.message_handler(state=Admin.add_price)
async def add_chatf2(message: types.Message, state: FSMContext):
    text = message.text.split(' ')
    if len(text) == 2:
        if text[0].isdigit() and text[1].isdigit():
            async with state.proxy() as data:
                db = await add_price(data['chat'] , text[0], text[1])
            if db:
                ans = 'Список цен на подписки:'
                for i in db:
                    ans += f'\n{i[2]} дн. - {i[3]} руб.'
            else:
                ans = 'Список цен пуст'
            kb = await kb_edit()
            await message.answer(ans, reply_markup=kb)
            await Admin.price_chat.set()
        else:
            message.answer('Данные должны быть целыми числами')
    else:
        message.answer('Неправильный ввод')


@dp.callback_query_handler(lambda c: c.data == 'delete', state=Admin.price_chat)
async def del_chatf(call: types.CallbackQuery, state: FSMContext):
    async with state.proxy() as data:
        kb = await kb_prices(data['chat'])
    await call.message.edit_text('Выберите пункт', reply_markup=kb)
    await Admin.delete_price.set()


@dp.callback_query_handler(lambda c: c.data.startswith('price'), state=Admin.delete_price)
async def del_chatf(call: types.CallbackQuery, state: FSMContext):
    async with state.proxy() as data:
        db = await del_price(data['chat'], call.data[5:])
    if db:
        ans = 'Список цен на подписки:'
        for i in db:
            ans += f'\n{i[2]} дн. - {i[3]} руб.'
    else:
        ans = 'Список цен пуст'
    kb = await kb_edit()
    await call.message.edit_text(ans, reply_markup=kb)
    await Admin.price_chat.set()
