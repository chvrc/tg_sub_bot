import time
from aiogram.dispatcher.storage import FSMContext
from database import get_price, sub_add
from keyboards import kb_back, kb_pay, kb_prices
from states import MainMenu
from load_bot import dp
from aiogram import types
from payments import pay_subscribe_check


@dp.callback_query_handler(lambda c: c.data == 'subscribe', state=MainMenu.subscribe)
async def on_chats_action(call: types.CallbackQuery, state: FSMContext):
    async with state.proxy() as data:
        kb = await kb_prices(data['chat'])
    await call.message.edit_text('Выберите вариант подписки',
                            reply_markup=kb)


@dp.callback_query_handler(lambda c: c.data.startswith('price'), state=MainMenu.subscribe)
async def on_chats_action(call: types.CallbackQuery, state: FSMContext):
    db = await get_price(call.data[5:])
    async with state.proxy() as data:
        data['price'] = call.data[5:]
        hash = f"{call.from_user.id}{int(time.time())}"
    kb = await kb_pay(hash, db[3])
    await call.message.edit_text('<b>Важно!</b> После оплаты перейдите обратно в бота и нажмите <b>Подтвердить оплату</b>',
                            reply_markup=kb, parse_mode='HTML')


@dp.callback_query_handler(lambda c: c.data.startswith('paycheck'), state=MainMenu.subscribe)
async def on_start_action(call: types.CallbackQuery, state: FSMContext):
    check = pay_subscribe_check(call.data[8:])
    if check:
        async with state.proxy() as data:
            db = await get_price(data['price'])
        await sub_add(call.from_user.id, db[2], db[1])
        kb = await kb_back()
        await call.message.edit_text('Успешная оплата!', reply_markup=kb)
        await state.finish()
    else:
        await dp.bot.answer_callback_query(callback_query_id=call.id,
                        text='Вы не оплатили подписку', show_alert=True)
