import sqlite3
import time
import datetime

async def create_db():
    conn = sqlite3.connect('subs.db')
    cur = conn.cursor()
    cur.execute("""
                    CREATE TABLE IF NOT EXISTS chats(
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    title TEXT,
                    chat_id INTEGER NOT NULL UNIQUE);
                """)
    cur.execute("""
                    CREATE TABLE IF NOT EXISTS subs(
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    uid INTEGER NOT NULL,
                    sub_time INTEGER,
                    chat INTEGER,
                    FOREIGN KEY(chat) REFERENCES chats(id));
                """)
    cur.execute("""
                    CREATE TABLE IF NOT EXISTS prices(
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    chat INTEGER,
                    sub_time INTEGER,
                    price INTEGER,
                    FOREIGN KEY(chat) REFERENCES chats(id));
                """)
    cur.execute("""
                    CREATE TABLE IF NOT EXISTS referals(
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    uid INTEGER NOT NULL UNIQUE,
                    ref_count INTEGER);
                """)
    conn.commit()


async def get_chats():
    conn = sqlite3.connect('subs.db')
    cur = conn.cursor()
    cur.execute("""
                SELECT * FROM chats
                """)
    res = cur.fetchall()
    conn.commit()
    return res


async def get_chat_id(id):
    conn = sqlite3.connect('subs.db')
    cur = conn.cursor()
    cur.execute("""
                SELECT chat_id FROM chats
                WHERE id = ?
                """, (id,))
    res = cur.fetchall()
    conn.commit()
    return res[0][0]


async def add_chat(title, chat_id):
    conn = sqlite3.connect('subs.db')
    cur = conn.cursor()
    cur.execute("""
                INSERT OR REPLACE INTO chats(title, chat_id) VALUES(?, ?);
                """, (title, f'-100{chat_id}'))
    cur.execute("""SELECT * from chats;
                    """)
    res = cur.fetchall()
    conn.commit()
    return res


async def del_chat(id):
    conn = sqlite3.connect('subs.db')
    cur = conn.cursor()
    cur.execute("""DELETE FROM chats
                    WHERE id = ?;
                    """, (id, ))
    cur.execute("""SELECT * from chats;
                    """)
    res = cur.fetchall()
    conn.commit()
    return res


async def get_prices(chat):
    conn = sqlite3.connect('subs.db')
    cur = conn.cursor()
    cur.execute("""
                SELECT * FROM prices
                WHERE chat = ?
                """, (chat,))
    res = cur.fetchall()
    conn.commit()
    return res


async def get_price(id):
    conn = sqlite3.connect('subs.db')
    cur = conn.cursor()
    cur.execute("""
                SELECT * FROM prices
                WHERE id = ?
                """, (id, ))
    res = cur.fetchall()
    conn.commit()
    return res[0]


async def add_price(chat, sub_time, price):
    conn = sqlite3.connect('subs.db')
    cur = conn.cursor()
    cur.execute("""
                INSERT OR REPLACE INTO prices(chat, sub_time, price) VALUES(?, ?, ?);
                """, (chat, sub_time, price))
    cur.execute("""
                SELECT * FROM prices
                WHERE chat = ?
                """, (chat,))
    res = cur.fetchall()
    conn.commit()
    return res


async def del_price(chat, id):
    conn = sqlite3.connect('subs.db')
    cur = conn.cursor()
    cur.execute("""DELETE FROM prices
                    WHERE id = ?;
                    """, (id, ))
    cur.execute("""
                SELECT * FROM prices
                WHERE chat = ?
                """, (chat,))
    res = cur.fetchall()
    conn.commit()
    return res


async def sub_add(uid, ttime, chat):
    sub_time = int(time.time() + 60 * 60 * 24 * ttime)
    conn = sqlite3.connect('subs.db')
    cur = conn.cursor()
    cur.execute("""
                INSERT OR IGNORE INTO subs(uid, sub_time, chat) VALUES(?, ?, ?);
                """, (uid, sub_time, chat))
    conn.commit()


async def sub_delete(uid, chat):
    conn = sqlite3.connect('subs.db')
    cur = conn.cursor()
    cur.execute("""
                DELETE from subs
                WHERE uid=? AND chat=?;
                """, (uid, chat))
    conn.commit()


async def sub_check(uid, chat):
    conn = sqlite3.connect('subs.db')
    cur = conn.cursor()
    cur.execute("""
                SELECT sub_time FROM subs
                WHERE uid=? AND chat=?;
                """, (uid, chat))
    res = cur.fetchall()
    conn.commit()
    if res:
        if int(res[0][0]) > int(time.time()):
            return True
        else:
            await sub_delete(uid, chat)
            return False
    else:
        return False


async def sub_remain_time(uid, chat):
    conn = sqlite3.connect('subs.db')
    cur = conn.cursor()
    cur.execute("""
                SELECT sub_time FROM subs
                WHERE uid=? AND chat=?;
                """, (uid, chat))
    res = cur.fetchall()
    conn.commit()
    seconds = res[0][0] - int(time.time())
    return datetime.timedelta(seconds=seconds)


async def add_referal(uid1, uid2):
    conn = sqlite3.connect('subs.db')
    cur = conn.cursor()
    cur.execute("""
                    INSERT OR IGNORE INTO referals(uid, ref_count) VALUES(?, ?);
                """, (uid1, 0))
    cur.execute("""
                    INSERT OR IGNORE INTO referals(uid, ref_count) VALUES(?, ?);
                """, (uid2, 1))
    cur.execute("""
                    UPDATE referals
                    SET ref_count = (ref_count + 1)
                    WHERE uid = ?
                """, (uid1, ))
    res = cur.fetchall()
    conn.commit()
    return res


async def edit_referal(uid):
    conn = sqlite3.connect('subs.db')
    cur = conn.cursor()
    cur.execute("""
                    UPDATE referals
                    SET ref_count = (ref_count - 1)
                    WHERE uid = ?
                """, (uid, ))
    res = cur.fetchall()
    conn.commit()
    return res


async def check_referal(uid):
    conn = sqlite3.connect('subs.db')
    cur = conn.cursor()
    cur.execute("""
                    SELECT * FROM referals
                    WHERE uid = ?
                """, (uid, ))
    res = cur.fetchall()
    conn.commit()
    return res