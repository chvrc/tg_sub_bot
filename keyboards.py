from load_bot import bot
from database import get_chats, get_prices
from aiogram.types import (InlineKeyboardMarkup, InlineKeyboardButton, 
                            ReplyKeyboardMarkup, KeyboardButton)

from payments import pay_subscribe


async def kb_admin():
    bt1 = InlineKeyboardButton('Группы', callback_data='admin_chat')
    bt2 = InlineKeyboardButton('Подписки', callback_data='admin_price')
    bt3 = InlineKeyboardButton('Выйти', callback_data='admin_exit')
    kb = InlineKeyboardMarkup()
    kb.row(bt1, bt2).add(bt3)
    return kb


async def kb_edit():
    bt1 = InlineKeyboardButton('Добавить', callback_data='add')
    bt2 = InlineKeyboardButton('Удалить', callback_data='delete')
    bt0 = InlineKeyboardButton('Назад ⬅️', callback_data='back')
    kb = InlineKeyboardMarkup()
    kb.row(bt1, bt2).add(bt0)
    return kb


async def kb_chats():
    db = await get_chats()
    kb = InlineKeyboardMarkup()
    for i in db:
        bt = InlineKeyboardButton(f'{i[1]}', callback_data=f'chat{i[0]}')
        kb.add(bt)
    bt0 = InlineKeyboardButton('Назад ⬅️', callback_data='back')
    kb.add(bt0)
    return kb


async def kb_prices(chat):
    db = await get_prices(chat)
    kb = InlineKeyboardMarkup()
    for i in db:
        bt = InlineKeyboardButton(f'{i[2]} дн. [{i[3]} RUB]', callback_data=f'price{i[0]}')
        kb.add(bt)
    bt0 = InlineKeyboardButton('Назад ⬅️', callback_data='back')
    kb.add(bt0)
    return kb


async def kb_back():
    bt0 = InlineKeyboardButton('Назад ⬅️', callback_data='back')
    kb = InlineKeyboardMarkup()
    kb.add(bt0)
    return kb


async def kb_post_back(remain_sub_time):
    bt1 = InlineKeyboardButton(f'Оставшееся время подписки: {remain_sub_time}', callback_data='remain_time')
    bt0 = InlineKeyboardButton('Назад ⬅️', callback_data='back')
    kb = InlineKeyboardMarkup()
    kb.add(bt1).add(bt0)
    return kb


async def kb_back_start():
    bt0 = InlineKeyboardButton('В главное меню 🏠', callback_data='start')
    kb = InlineKeyboardMarkup()
    kb.add(bt0)
    return kb


async def kb_start():
    bt1 = InlineKeyboardButton('Разместить обьявление 📨', callback_data='groups')
    bt2 = InlineKeyboardButton('Правила размещения 🗞', callback_data='rules')
    bt3 = InlineKeyboardButton('Реферальная программа 🆓', callback_data='referals')
    kb = InlineKeyboardMarkup()
    kb.add(bt1).add(bt2).add(bt3)
    return kb


async def kb_referal():
    bt1 = InlineKeyboardButton('Пригласить рефералов 🆓', callback_data='referals')
    bt2 = InlineKeyboardButton('Назад ⬅️', callback_data='back')
    kb = InlineKeyboardMarkup()
    kb.add(bt1).add(bt2)
    return kb


async def kb_posted(chat_id):
    info = await bot.get_chat(chat_id)
    url = await info.get_url()
    bt1 = InlineKeyboardButton('Перейти на канал', url=url)
    bt2 = InlineKeyboardButton('Назад ⬅️', callback_data='back')
    kb = InlineKeyboardMarkup()
    kb.add(bt1).add(bt2)
    return kb


async def kb_post():
    bt1 = InlineKeyboardButton('Купить подписку 💴', callback_data='subscribe')
    bt2 = InlineKeyboardButton('Бесплатное размещение 🆓', callback_data='referal')
    bt0 = InlineKeyboardButton('Назад ⬅️', callback_data='back')
    kb = InlineKeyboardMarkup()
    kb.add(bt1).add(bt2).add(bt0)
    return kb


async def kb_pay(hash, price):
    url = pay_subscribe(hash, price)
    bt1 = InlineKeyboardButton('Оплатить [YooMoney] 💴', url=url)
    bt2 = InlineKeyboardButton('Подтвердить оплату ✅', callback_data=f'paycheck{hash}')
    bt0 = InlineKeyboardButton('Назад ⬅️', callback_data='back')
    kb = InlineKeyboardMarkup()
    kb.add(bt1).add(bt2).add(bt0)
    return kb


async def kb_skip():
    bt1 = KeyboardButton('Пропустить')
    kb = ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
    kb.add(bt1)
    return kb
    
